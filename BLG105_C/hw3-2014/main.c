#include <stdio.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>
/*
	 ___________________________________________________________
	/															\
	|	@name			=> Silhoutte							|
	|	@description	=> Homework project for BIL105 lecture	|
	|	@author			=> Emin Mastizada						|
	|	@studentNo		=> 150120914							|
	|	@university		=> Istanbul Technical university 		|
	\___________________________________________________________/

	@input -> buildings.txt
	@output -> outline.txt
*/

// Sorter - Sorts 2D arrays
void sort(int data[][3], int N) {
	if (N<=1) return; 	/// Array with 1 int or empty is already sorted
	int i, j, v, t;
	v = data[0][0];
	i = 0;
	j = N;
	for(;;) {
	/*
	  idea of algorithm by: Francisco Rosales
	  mod for first item of 2D data: Emin Mastizada
	*/
	while(i < N && data[++i][0] < v) { }
	while(data[--j][0] > v) { }
	if(i >= j) break;
	t = data[i][0]; data[i][0] = data[j][0]; data[j][0] = t;
	}
	t = data[i-1][0]; data[i-1][0] = data[0][0]; data[0][0] = t;
	sort(data, i-1);
	sort(data+i, N-i);
}
void sorter(int data[][3], int N) {
	int i,j;
	int _data[N][3];
	int temp[N][3];
	memcpy(_data, data, sizeof (int) * N * 3);
	memcpy(temp, data, sizeof (int) * N * 3);
	sort(_data, N);
	for (i=0; i<N;i++) {
		for (j=0; j<N; j++) {
			if(temp[j][0] == _data[i][0]) {
				data[i][0] = temp[j][0];
				data[i][1] = temp[j][1];
				data[i][2] = temp[j][2];
				break;
			}
		}
	}
}

int main(void) {
	int N = 0, ch, i;
	FILE *inFile = fopen("buildings.txt", "r");	// Open input file for read
	if (inFile == NULL) {
		printf("File 'buildings.txt' not found!\n");
		return 1;
	}
	FILE *outFile = fopen("outline.txt", "w"); 	// Create new file for output
	//Get total number of lines:
	do {
		ch = fgetc(inFile);
		if (ch == '\n')
			N++;
	}  while (ch != EOF);
	rewind(inFile);				/// Return back to top of file
	int buildings[N][3];		/// Array for building
	// 0 - start, 1 - height, 2 - width
	memset(buildings, 0, N*3*sizeof(int));	// Set it to memory, make all 0 to prevent possible errors
	/// Read lines and assign them to array:
	for(i = 0; i < N; i++)
		fscanf(inFile, "%d\t%d\t%d", &buildings[i][0], &buildings[i][1], &buildings[i][2]);
	fclose(inFile);	// Reading done

	/*for (i=0;i<N;i++) {
		printf("%d\n", buildings[i][0]);
	}*/		/// Test print for testing sort func
	///printf("After sort: \n");
	/// Lets sort them:
	sorter(buildings, N);
	/*for (i=0;i<N;i++) {
		printf("%d\n", buildings[i][0]);
	}*/		/// Test print for testing sort func
	/// Convert width to end point:
	for (i=0; i<N; i++)
		buildings[i][2] = buildings[i][0] + buildings[i][2];
	/*for (i=0;i<N;i++) {
		printf("%d-%d-%d\n", buildings[i][0], buildings[i][1], buildings[i][2]);
	}*/		/// Test print for testing sort func
	int silhoutte[N*2][2];
	memset(silhoutte, 0, N*2*2*sizeof(int));
	silhoutte[0][0] = buildings[0][0];	/// x
	silhoutte[0][1] = 0; 				/// y
	silhoutte[1][0] = buildings[0][0]; // x
	silhoutte[1][1] = buildings[0][1]; // z
	silhoutte[2][0] = buildings[0][2]; // x
	silhoutte[2][1] = buildings[0][1]; // z
	int j = 3; //for silhoutte
	for (i=1; i<N; i++) {
		if (buildings[i][0] > buildings[i-1][2] && buildings[i][0] < buildings[i+1][0]) {
			silhoutte[j][0] = buildings[i][0];
			silhoutte[j++][1] = 0;
		} else {		// Start points
			if (buildings[i-1][1] > buildings[i][1] && buildings[i-1][2] < buildings[i][2]) {
				silhoutte[j][0] = buildings[i-1][2];
				silhoutte[j++][1] = buildings[i][1];
				/*printf("%d - %d\n", buildings[i-1][2], buildings[i][1]);*/
			}
		}
		if (buildings[i][2] > buildings[i-1][2] && buildings[i][2] < buildings[i+1][0]) {
			silhoutte[j][0] = buildings[i][2];
			silhoutte[j++][1] = 0;
		} else {	// End points

		}
		if (buildings[i][1] > buildings[i-1][1] && buildings[i][1] > buildings[i+1][1]) {
			silhoutte[j][0] = buildings[i][0];
			silhoutte[j++][1] = buildings[i][1];
			silhoutte[j][0] = buildings[i][2];
			silhoutte[j++][0] = buildings[i][1];
		} else if (buildings[i][1] > buildings[i-1][1] && buildings[i][0] < buildings[i+1][0]) {
			silhoutte[j][0] = buildings[i][0];
			silhoutte[j++][1] = buildings[i][1];
		} else if (buildings[i][1] > buildings[i+1][1]) {
			if (i<N-2) {
				if (buildings[i][2] < buildings[i+2][1]) {
					silhoutte[j][0] = buildings[i][2];
					silhoutte[j++][1] = buildings[i][1];
				}
			}
		}
		if (buildings[i-1][2] > buildings[i][0] && buildings[i-1][1] < buildings[i][1]) {
			silhoutte[j][0] = buildings[i][0];
			silhoutte[j++][1] = buildings[i-1][1];
		}
		if (buildings[i-1][1] < buildings[i][1]) {
			if (buildings[i][0] > buildings[i+1][0]) {
				silhoutte[j][0] = buildings[i][0];
				silhoutte[j++][1] = buildings[i][1];
			} else {
				if (buildings[i][1] > buildings[i+1][1]) {
					silhoutte[j][0] = buildings[i][0];
					silhoutte[j++][1] = buildings[i][1];
				}
			}
		}
		if (buildings[i][1] > buildings[i+1][1] && buildings[i][2] > buildings[i-1][2]) {
			silhoutte[j][0] = buildings[i][2];
			silhoutte[j++][1] = buildings[i][1];
		}
	}
	silhoutte[j][0] = buildings[N-1][2];
	silhoutte[j++][1] = 0;
	for(i=0; i<j; i++) {
		///printf("(%d, %d)\n", silhoutte[i][0], silhoutte[i][1]);
		fprintf(outFile, "(%d,%d)", silhoutte[i][0], silhoutte[i][1]);
		if (i != (j-1))
			fprintf(outFile, ",");
		else
			fprintf(outFile, "\n");
	}
	fclose(outFile);
	return 0;
}