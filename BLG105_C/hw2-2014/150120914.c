#include <stdio.h>

#define BufferSize 250

void printItems(const char itemNames[7][BufferSize], const int itemPrices[7], int totalItems) {
	int i=0;
	float price;
	printf("\nAVAILABLE ITEMS\n");
	printf("(Item#\tItemName\tItemPrice)\n");
	for(i=0; i<totalItems; i++) {
		price = itemPrices[i] / 100.0;
		if (i==3) {
			printf("%6d\t%s\t%.2f TL\n", i+1, itemNames[i], price);
		} else {
		printf("%6d\t%s\t\t%.2f TL\n", i+1, itemNames[i], price);
		}
	}
}

void getCoins(int *totalPrice) {
	char userInput[BufferSize];
	char *jump;
	int coin;
	int offset; // Count of bytes to advance after each sscanf
	printf("Enter Coins => ");
	fgets(userInput, BufferSize, stdin);
	jump = userInput; // Beginning address.
	while (sscanf(jump, "%d%n", &coin, &offset) == 1) {
		jump += offset;
		if (coin==1) {
			(*totalPrice) += 100;
		} else if(coin==100) {
			(*totalPrice) += 100;
		} else if(coin==5) {
			(*totalPrice) += 5;
		} else if(coin==10) {
			(*totalPrice) += 10;
		} else if(coin==25) {
			(*totalPrice) += 25;
		} else if(coin==50) {
			(*totalPrice) += 50;
		} else {
			printf("Invalid Currency:\t%d Krş\n", coin);
		}
	}
}

int getItem(int total) {
	int item;
	printf("\nEnter your item selection => ");
	scanf("%d", &item);
	if (item > 0 && item <= total) {
		return item-1;
	} else {
		printf("No such item\n");
		return -1;
	}	
}

void discharge(const char name[], int itemPrice, int total) {
	float price;
	int extra;
	printf("%s has been discharged:\t", name);	// Print name
	if (itemPrice >= 100) {				// Print item price in Krs or TL
		price = itemPrice / 100.0;
		printf("%.2f TL\n", price);
	} else {
		printf("%d Krş\n", itemPrice);
	}
	if (total >= 100) {				// Print user total input in Krs or TL
		price = total / 100.0;
		printf("You have deposited total:\t%.2f TL\n", price);
	} else {
		printf("You have deposited total:\t%d Krş\n", total);
	}
	extra = total - itemPrice;
	if (extra < 0) {
		printf("You don't have enough money for this item.\nWe are not charity organization :D\n");
	} else {
		printf("Please take back your remaining coins:\t%d Krş (", extra);
		while (extra != 0) {
			if(extra>=100) {
				printf("1 TL");
				extra -= 100;
				if (extra>0) {
					printf(" + ");
				}
			} else if(extra>=50) {
				printf("50");
				extra -= 50;
				if (extra>0) {
					printf(" + ");
				}
			} else if(extra>=25) {
				printf("25");
				extra -= 25;
				if (extra>0) {
					printf(" + ");
				}
			} else if(extra>=10) {
				printf("10");
				extra -= 10;
				if (extra>0) {
					printf(" + ");
				}
			} else if(extra>=5) {
				printf("5");
				extra -= 5;
				if (extra>0) {
					printf(" + ");
				}
			} else {
				printf("Error, extra=%d\n", extra);
			}
		}
		printf(")\n");
	}
	// 20 Krş (10 + 10)

}

int main() {
	int item, totalPrice = 0;
	const char itemNames[7][BufferSize] = {"Water", "Cola", "Fanta", "Chocolate", "Biscuit", "Gum", "Candy"};
	const int itemPrices[7] = {55, 80, 100, 120, 95, 40, 60};	//Prices and names can not be changed
	int totalItems = 7;

	printItems(itemNames, itemPrices, totalItems);
	item = getItem(totalItems);
	if (item == -1) {
		return 1;
	}
	getchar();	// get out \n from stdin for fgets
	getCoins(&totalPrice);
	printf("\n");
	discharge(itemNames[item], itemPrices[item], totalPrice);
	return 0;
}