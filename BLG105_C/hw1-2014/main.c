/*********************************
 *		Triangle Calculator		*
 *			150120914			*
 *		Emin Mastizada			*
**********************************/
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define BufferSize 1000
float EPSILON = 1/1048576;

long getTriangleNumber() {
	int notFinished = 1;
	while(notFinished) {
		char triangleNumber[BufferSize];
		long triangleNum;
		fgets(triangleNumber, BufferSize, stdin);
		triangleNum = atol(triangleNumber);
		if (triangleNum == 0 || triangleNum < 0){
			printf("\nInput Error! Please enter !number! more than 0. Negative numbers are not higher than 0 too ;)\n");
			printf("Please, enter number of triangles:\t");
		} else {
			notFinished = 0;
			return (int) triangleNum;
		}
	}
}
float getTriangleCoordinate(){
	char triangleCoordinates[BufferSize];
	double triangleCoords;
	fgets(triangleCoordinates, BufferSize, stdin);
	triangleCoords = atof(triangleCoordinates);
	if (triangleCoords == 0)
		printf("0\n");
	return (int) triangleCoords;
}

float lengthCount(float x1,float y1,float x2,float y2) {
	return sqrt(pow((x1-x2),2) + pow((y1-y2),2));

}
int isTriangle(float a, float b, float c){
	int result;
	if (fabsf(b-c) < a && (b+c) > a && fabsf(a-c) < b && (a+c) > b && fabsf(a-b) < c && (a+b) > c)
		return 1;
	else
		return 0;

}
float triangleArea(float x1, float x2, float x3, float y1, float y2, float y3) {
	return (x1*(y2-y3)+x2*(y3-y1)+x3*(y1-y2)) / 2;
}
float COG(float a, float b, float c) {
	return (a+b+c) / 3;
}
float median(float a, float b, float c) {
	return (sqrt(2*(b*b + c*c)-a*a)) / 2;
}
void triangleType(float a, float b, float c) {
	if (fabs(a-b) <= EPSILON && fabs(b-c) <= EPSILON && fabs(a-c) <= EPSILON ) {
		printf("Regular Triangle\n");
	} else if ((fabs(a-b) <= EPSILON && fabs(b-c) > EPSILON) || (fabs(a-c) <= EPSILON && fabs(c-b) > EPSILON ) || (fabs(b-c) <= EPSILON && fabs(a-c) > EPSILON )) {
		printf("Isosceles Triangle\n");
	} else if (fabs((a*a + b*b) - c*c) <= EPSILON || fabs((a*a + c*c) - b*b) <= EPSILON || fabs((b*b + c*c) - a*a) <= EPSILON ){
		printf("Right Triangle\n");
	} else if (fabs(a-b) > EPSILON || fabs(b-c) > EPSILON || fabs(a-c) > EPSILON ) {
		printf("Scalene Triangle\n");
	}
}
int main() {
	float x[BufferSize][BufferSize], y[BufferSize][BufferSize];		// X and Y coordinates of triangles.
	float a, b, c;		// Distance
	int triangleNumber;	/// Number of Triangle to be count.
	int i, j;	/// For loops and etc.
	printf("Please, enter number of triangles:\t");
	triangleNumber = getTriangleNumber();
	printf("Enter coordinates for %d Triangles:\n\n", triangleNumber);
	for (i=1;i<=triangleNumber;i++) {
		printf("Enter x and y for %d.triangle:\n", i);
		for (j=1;j<=3;j++) {
			printf("x%d: ", j);
			x[i-1][j-1] = getTriangleCoordinate();
			printf("y%d: ", j);
			y[i-1][j-1] = getTriangleCoordinate();
		}
	}
	for (i=1;i<=triangleNumber;i++){
		printf("\nTriangle %d:\n", i);
		a = lengthCount(x[i-1][0], y[i-1][0], x[i-1][1], y[i-1][1]);
		b = lengthCount(x[i-1][0], y[i-1][0], x[i-1][2], y[i-1][2]);
		c = lengthCount(x[i-1][1], y[i-1][1], x[i-1][2], y[i-1][2]);
		printf("Length of a: %6.4f\n", a);
		printf("Length of b: %6.4f\n", b);
		printf("Length of c: %6.4f\n", c);
		if (isTriangle(a, b, c)){
			printf("Area of triangle: %6.4f\n", triangleArea(x[i-1][0], x[i-1][1], x[i-1][2], y[i-1][0], y[i-1][1], y[i-1][2]));
			printf("Center of Gravity - Cx: %6.4f, Cy: %6.4f\n", COG(x[i-1][0], x[i-1][1], x[i-1][2]), COG(y[i-1][0], y[i-1][1], y[i-1][2]));
			printf("Median a: %6.4f\n", median(a,b,c));
			printf("Median b: %6.4f\n", median(b,a,c));
			printf("Median c: %6.4f\n", median(c,a,b));
			printf("Triangle type: ");
			triangleType(a,b,c);
		} else {
			printf("\nInvalid triangle!\n\nTriangle %d does not hold to Triangle Inequality Rule\n", i);
		}
	}
	return 0;

}