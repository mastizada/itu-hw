/// We don't want to include it twice and make conflict:
#ifndef TRAINEE_H_
#define TRAINEE_H_
class Treadmill;
class Trainee {
	int size;
	const std::string name;
	int age;
	const bool gender;	// 1:female 0:male
	int height;	// in cm
	int weight;	//in grams
	int dailyActivityDuration;	// in minutes
	int numOfTreadmills;
	int totalCaloriesBurned;	//kcal
	Treadmill** treadmillList;
public:
	Trainee(const std::string &, int, const bool, int, int, int dailyActivityDuration = 30);
	void printDetails();
	bool addTreadmill(Treadmill *Obj);
	void exercise();
	Trainee(const Trainee &);	//Copy constructor
	void looseWeight();
	~Trainee();
};
#endif /*TRAINEE_H_*/
