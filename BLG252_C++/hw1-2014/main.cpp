#include <iostream>
#include "trainee.h"
#include "treadmill.h"
// Other includes
using namespace std;

int Treadmill::numOfTreadmills;

int main(int argv, char * argc[]) {
	Treadmill::resetNumOfTreadmills(); // Resetting number of treadmills to 0.
	// Creating Treadmills for the gym:
	Treadmill tM1;
	Treadmill tM2;
	Treadmill tM3;
	Treadmill tM4; // Check constructor/destructor messages
	{
		Treadmill tM5;
	}
	Treadmill tM6;
	// Creating a Trainee: (dailyActivityDuration is 30 minutes as default) // Check Constructor messages
	Trainee trainee1 ("Sally Brown", 18, 1, 170, 55000);
	//Trainee trainee2 = trainee1;		// When uncommented, should work properly
	// Try a daily exercise before treadmill/s are related with this trainee:
	trainee1.exercise();	// Check error message
	// //Adding treadmill pointers to tradmill list of Trainee
	if (!trainee1.addTreadmill(&tM1)) cout << "Add Operation 1 is not successfull" << endl;
	if (!trainee1.addTreadmill(&tM2)) cout << "Add Operation 2 is not successfull" << endl;
	if (!trainee1.addTreadmill(&tM3)) cout << "Add Operation 3 is not successfull" << endl;
	// // Should print out error message since max list size is 3.
	if (!trainee1.addTreadmill(&tM4)) cout << "Add Operation 4 is not successfull" << endl << endl;
	// // Try to loose weight before exercise :
	trainee1.looseWeight();
	// // SAMPLE Workout:
	trainee1.exercise();
	// // Update Weight:
	trainee1.looseWeight();
	// // What if all treadmills are unavailable?
	tM1.unsetAvailability();
	tM2.unsetAvailability();
	tM3.unsetAvailability();
	trainee1.exercise();	// Check error message
	return 0;
}
