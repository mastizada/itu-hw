#include <iostream>
#include <string>
#include "treadmill.h"
#include "trainee.h"

#define LISTSIZE 3

using namespace std;
// Constructor for Trainee:
Trainee::Trainee(const string &name, int age, const bool gender, int height, int weight, int dailyActivityDuration):name(name),gender(gender) {
	this->age = age;
	this->height = height;
	this->weight = weight;
	this->dailyActivityDuration = dailyActivityDuration;
	totalCaloriesBurned = 0;
	numOfTreadmills = 0;
	treadmillList = new Treadmill*[LISTSIZE];
	printDetails();
}
Trainee::Trainee(const Trainee& Obj):name(Obj.name),gender(Obj.gender) {
	age = Obj.age;
	height = Obj.height;
	weight = Obj.weight;
	dailyActivityDuration = Obj.dailyActivityDuration;
	totalCaloriesBurned = 0;
	numOfTreadmills = 0;
	treadmillList = new Treadmill*[LISTSIZE];
	printDetails();
}
// Print details of Trainee:
void Trainee::printDetails() {
	cout << "\nA ";
	if(gender==false) {
		cout << "male";
	} else {
		cout << "female";
	}
	cout << " trainee is created:\n";
	cout << "AGE\t\t: " << age << endl;
	cout << "Name\t\t: " << name << endl;
	cout << "Gender\t\t: " << gender << endl;
	cout << "Height\t\t: " << height << endl;
	cout << "Weight\t\t: " << weight << endl;
	cout << "Duration\t: " << dailyActivityDuration << endl << endl;
}
bool Trainee::addTreadmill(Treadmill *Obj) {
	bool inArray = false;
	for (int i=0;i<numOfTreadmills;i++) {
		if(treadmillList[i]->getId() == Obj->getId()) {
			inArray = true;	// Already added
		}
	}
	if (!inArray && numOfTreadmills < LISTSIZE) {
		treadmillList[numOfTreadmills++] = Obj;
		return true;
	} else {
		return false;
	}
}
void Trainee::looseWeight() {
	if (totalCaloriesBurned == 0) {
		cout << "Sorry, NO workout = NO LOSS" << endl << endl;
	} else {
		cout << "***** Weight UPDATE: *****" << endl;
		cout << "Previous weight was: " << weight << endl;
		weight -= 1.5 * totalCaloriesBurned + 0.5;
		totalCaloriesBurned = 0;
		cout << "NOW: " << weight << " kg..." << endl << endl;
	}
}
void Trainee::exercise() {
	bool available = false;
	int availableID;
	numOfTreadmills = this->numOfTreadmills;
	if (numOfTreadmills == 0) {
		cout << "Sorry, no treadmills are related to trainee: " << name << ", Can NOT exercise" << endl << endl;
	} else {
		for (int i=0; i<numOfTreadmills; i++) {
			if(this->treadmillList[i]->isAvailable()) {
				available = true;
				availableID = i;
				break;
			}
		}
		if (available) {
			cout << "An available treadmill is found having id: " << availableID + 1 << endl;
			cout << "Exercisizing for " << this->dailyActivityDuration << " minutes ..." << endl;
			int burned = this->treadmillList[availableID]->run(available, gender, age, weight/1000) * this->dailyActivityDuration;
			this->totalCaloriesBurned += burned;
			cout << "Total Kilocalories burned: " << burned << endl << endl;
		} else {
			cout << "Sorry, NO treadmills are AVAILABLE to: " << name << ", Can NOT exercise" << endl << endl;;
		}
	}
}
// Constructor for Treadmill:
Treadmill::Treadmill() {
	availability = true;
	id = numOfTreadmills + 1;
	numOfTreadmills++;
	cout << "Treadmill with id: " << id << " is created" << endl;
}
bool Treadmill::isAvailable() {
	return availability;
}
int Treadmill::getId() {
	return id;
}
int Treadmill::run(bool availability, bool gender, int age, int weight) {
	availability = false;
	int burned;
	if (gender==0) { //male:
		burned = (-55.0969 + 0.6309 * 100 + 0.1988 * weight + 0.2017 * age) / 4.184 + 0.5;
	} else { //female:
		burned = (-20.4022 + 0.4472 * 100 - 0.1263 * weight + 0.074 * age) / 4.184 + 0.5;
	}
	availability = true;
	return burned;
}
void Treadmill::unsetAvailability() {
	availability = false;
}
// Destructor for Trainee:
Trainee::~Trainee() {
	delete[] treadmillList;
}
// Destructor for Treadmill:
Treadmill::~Treadmill() {
	numOfTreadmills--;
}