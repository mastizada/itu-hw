// Class for Treadmill
#include "trainee.h"
class Treadmill {
	int id;
	bool availability;
	static int numOfTreadmills;
public:
	static void resetNumOfTreadmills() { numOfTreadmills = 0;}
	Treadmill();
	bool isAvailable();
	int getId();
	int run(bool, bool, int, int);
	void unsetAvailability();
	~Treadmill();
};