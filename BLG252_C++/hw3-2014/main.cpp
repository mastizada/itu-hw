#include <iostream>
#include <string>
#include <vector>
//Actually I wanted to use list, but its not in out lecture program:
// http://stackoverflow.com/a/2209564/3042057

using namespace std;

class Quote {
protected:
	string movie;
	string quote;
	string owner;
public:
	Quote();						/// Empty constructor
	Quote(string);					/// Constructor with quote
	Quote(string, string);			/// Constructor with quote, owner
	Quote(string, string, string);	/// Constructor with quote, owner, movie
	void set_quote(string);			/// Set quote
	void set_owner(string);			/// Set owner
	void set_movie(string);			/// Set movie
	string get_movie();				/// get title of movie
	void print();					/// Print quote details
};
/// Functions for Quote class
Quote::Quote() {};
Quote::Quote(string quote) {
	this->quote = quote;
}
Quote::Quote(string quote, string owner) {
	this->quote = quote;
	this->owner = owner;
}
Quote::Quote(string quote, string owner, string movie) {
	this->quote = quote;
	this->owner = owner;
	this->movie = movie;
}
void Quote::set_quote(string quote) {
	this->quote = quote;
}
void Quote::set_owner(string owner) {
	this->owner = owner;
}
void Quote::set_movie(string movie) {
	this->movie = movie;
}
void Quote::print() {
	cout << this->quote << endl;
	cout << "\t(" << this->owner << " - " << this->movie << ")\n";
}
string Quote::get_movie() {
	return this->movie;
}
/// end for Quete class function
class Movie {
	string movie;
	vector<Quote> quotes;	/// List of quotes
public:
	Movie();							/// Empty constructor
	Movie(string);						/// Constructor with ovie title
	void set_title(string);				/// Set movie title
	void operator+(Quote&);		/// + operator for Movie object
	void print();						/// Prints Quotes of movie
	~Movie();							/// Deconstructor - at the end of code
	
};
/// functions for Movie class
Movie::Movie() {}
Movie::Movie(string movie) {
	this->movie = movie;
}
void Movie::set_title(string movie) {
	this->movie = movie;
}
void Movie::operator+(Quote& Obj) {
	if (Obj.get_movie() != movie)	// Check if quotes title is belongs to this movie.
		throw "Insertion error: titles mismatch";	// return error message
	quotes.push_back(Obj);	// add quote to vector.
}
void Movie::print() {
	int i = 0;
	for (i=0;i<quotes.size();i++) {		// call print function of every object in vector.
		quotes.at(i).print();
	}
}
/// end for Movie class functions
int main() {
	Quote q1;
	q1.set_quote("It's a trap!");
	q1.set_owner("admiral ackbar");
	q1.set_movie("return of the jedi");
	Quote q2("eight year olds dude...", "walter sobchak", "the big lebowski");
	Quote q3("if you want to survive out here, you've got to know where your towel is.");
	q3.set_owner("ford prefect");
	q3.set_movie("the hitchhiker's guide to the galaxy");
	Quote q4("this will all end in tears.", "marvin", "the hitchhiker's guide to the galaxy");
	Quote q5("what does marcellus wallace look like?", "jules winfield", "pupl fiction");
	Movie m1;
	m1.set_title("reservoir dogs");
	Movie m2("the hitchhiker's guide to the galaxy");
	Movie m3("the big lebowski");
	try {m2+q3;} catch(const char *m) {cout << m << endl;}
	try {m2+q4;} catch(const char *m) {cout << m << endl;}
	try {m3+q2;} catch(const char *m) {cout << m << endl;}
	try {m1+q5;} catch(const char *m) {cout << m << endl;}
	m2.print();
	return 0;
}
/// Deconstructor for movie class
Movie::~Movie() {
	vector<Quote>().swap(quotes);
	// Clear will not deallocate memory,so after clear "sizeof(Type) * 10 bytes" stays in memory.
}